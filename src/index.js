//Moving on to ES6 style coding with Babel preset for node
// const fs = require('fs');
// const XmlStream = require('xml-stream');
// const req = require('request');

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/count';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/reduce';
import XmlStream from 'xml-stream';
import { FetchStream } from 'fetch';
import fs from 'fs-extra';
import path from 'path';
import Config from 'config';
import URL from 'url';

//STEP 1: Extract list of image files into an Observable
//Don't have AWS credentials; so any library that wraps s3 APIs  will not work
//import AWS from 'aws-sdk';


//const S3 = new AWS.S3();
// const response = S3.listObjectsV2(
// 		{
// 			Bucket:'waldo-recruiting'
// 		},
// 		(err,data) => {
// 			if(err) console.log(err, err.stack);
// 			else console.log(data);
// 		}
// 	);

//const xml$ = Rx.Observable.fromEvent(xml,'data');

//TODO: Account for MaxKeys and IsTruncated hooked to 'updateElement: ListBucketResult' event on XmlStream

//console.log(`Config object is: ${Config}`);

const url = URL.resolve(Config.get('Source.baseUrl'), Config.get('Source.bucketName'));
//console.log(`Source URL: ${url}`);
const tmpPath = path.join(Config.get('Target.tmpPath'),Config.get('Target.bucketName'));
//const tmpPath = 'tmp';
const xml$ = Observable
				.create(
					//next
					(observer) => {
						try {

							const xml = (new XmlStream(new FetchStream(url)));
							xml.on('updateElement: Contents', data => observer.next(data));
							xml.on('end', () => observer.complete());
							xml.on('updateElement: error', error => observer.error(error));
						} catch(err) {
							observer.error(err);
						}
					},
					//error
					(error) => {console.log(`Error: ${err}`)},
					//complete
					//() => {console.log(`Count: ${observer.count()}`)}

				)
				//.first();
				.take(5)

const totalFilesToDownload$ = xml$.count();
// xml$.subscribe(
// 		{
// 			next: x => console.log(x.Key),
// 			complete: () => xml$.count().subscribe(val => console.log(`Count: ${val}`)),
// 			error: (err) => console.log(`Error: ${err}`)
// 		}
// 	);

//STEP 2: Create an observable (image stream) that does parallelized fetch of images from S3 bucket
// Need to construct URL from url. This is simply url + file name (x.Key)
// Some files are .tar.gz files with potentially multiple images inside of them
//throttle by network bandwidth or some other Observable backpressure technique
//TODO: Retries if download fails the first time


//TODO: Account for API throttling as well as realtime available bandwidth.
// For the current use case this is not a problem because AWS S3 limits only at 300 GETs /sec
// and we are dealing with 121 concurrent downloads
// When designing a more dynamic system .groupBy() may turn out to be a better choice.
const tmpDir = fs.ensureDir(tmpPath, err => {if(err) console.log(`Error creating tmp directory: ${err}`)});
const file$ = xml$.mergeMap(
					xml => Observable.create(
						//next
						(observer) => {
							try{

								console.log(`Fetching: ${url}/${xml.Key}; Expected Size: ${xml.Size}`);
								const outFileName = `${tmpPath}/${xml.Key}`;
								const outFile = fs.createWriteStream(outFileName);
								const file = new FetchStream(`${url}/${xml.Key}`);
								file.pipe(outFile);
//								outFile.on('finish', () => console.log(`Fetched: ${xml.Key}; Size at finish: ${fs.statSync(outFileName).size}`));
//								file.on('data', data => observer.next(data));
//TODO: if xml.Size !== fs.statSync(outFileName).size, then put this file in retry queue
								outFile.on('finish', () => {
														console.log(`Fetched: ${xml.Key}; Size at finish: ${fs.statSync(outFileName).size}`);
														observer.complete();
													}
										);
								outFile.on('error', error => observer.error(error));
								file.on('error', error => observer.error(error));
							} catch(err){
								observer.error(err);
							}
						},
						//error
						(error) => console.log(`Error: ${err}`),
						//complete
						() => console.log(`Done!`)
					),
				);
const filesActuallyDownloaded$ = file$.count();
file$.subscribe(
		{
			next: x => console.log(`Chunk Size: ${x.length}`),
			complete: () => {
				totalFilesToDownload$.subscribe(val => console.log(`Total number of expected files to download: ${val}`).unsubscribe());
//				filesActuallyDownloaded$.subscribe(val => console.log(`Total number of actually downloaded: ${val}`).unsubscribe());
			},
			error: (err) => console.log(`Error: ${err}`)
		}
	);
//STEP3: Extract EXIF data from images in the image stream; commit to database
//Parallelized DB connection; throttled by connection pool

